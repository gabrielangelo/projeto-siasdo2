#!/bin/bash
set -e
LOGFILE=/home/ubuntu/logs/siasdo_gunicorn.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=6
USER=ubuntu
GROUP=ubuntu
ADDRESS=127.0.0.1:8000
cd /home/ubuntu/projeto-siasdo2/
exec gunicorn siasdo.wsgi:application 
