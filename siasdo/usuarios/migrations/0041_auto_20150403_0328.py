# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0040_auto_20150331_2329'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Atendimento',
        ),
        migrations.RemoveField(
            model_name='consulta',
            name='paciente',
        ),
        migrations.RemoveField(
            model_name='consulta',
            name='profissional',
        ),
        migrations.RemoveField(
            model_name='consultas_geral',
            name='paciente',
        ),
        migrations.RemoveField(
            model_name='consultas_geral',
            name='profissional',
        ),
        migrations.RemoveField(
            model_name='consultas_geral',
            name='servico',
        ),
        migrations.DeleteModel(
            name='Consulta',
        ),
        migrations.DeleteModel(
            name='Consultas_Geral',
        ),
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 3, 3, 28, 45, 29308)),
            preserve_default=True,
        ),
    ]
