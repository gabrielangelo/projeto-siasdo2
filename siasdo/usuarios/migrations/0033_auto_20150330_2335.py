# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0032_auto_20150330_2334'),
    ]

    operations = [
        migrations.AddField(
            model_name='bairro',
            name='estado',
            field=models.ForeignKey(default=0, to='usuarios.Estado'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 23, 35, 8, 838573)),
            preserve_default=True,
        ),
    ]
