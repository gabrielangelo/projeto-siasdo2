# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0022_auto_20150311_2209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 12, 1, 39, 39, 343954)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profissional',
            name='foto',
            field=models.ImageField(default=b'profile-none/profile_none.png', upload_to=b'profile-foto/'),
            preserve_default=True,
        ),
    ]
