# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0035_auto_20150330_2352'),
    ]

    operations = [
        migrations.AddField(
            model_name='bairro',
            name='cidade',
            field=models.ForeignKey(default=0, to='usuarios.Cidade'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 23, 52, 36, 236065)),
            preserve_default=True,
        ),
    ]
