# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0004_auto_20150310_0337'),
    ]

    operations = [
        migrations.AddField(
            model_name='profissional',
            name='foto',
            field=models.ImageField(default=b'siasdo/site_siasdo/static/assets/images/profile_none.png', upload_to=b'siasdo/site_siasdo/static/assets/images'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 10, 3, 38, 16, 759764)),
            preserve_default=True,
        ),
    ]
