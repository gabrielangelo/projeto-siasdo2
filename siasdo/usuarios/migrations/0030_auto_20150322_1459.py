# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0029_auto_20150322_1424'),
    ]

    operations = [
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 22, 14, 59, 51, 61175)),
            preserve_default=True,
        ),
    ]
