# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0023_auto_20150312_0139'),
    ]

    operations = [
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 12, 5, 43, 47, 217568)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profissional',
            name='curriculo',
            field=models.FileField(upload_to=b'curriculos/', blank=True),
            preserve_default=True,
        ),
    ]
