# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0034_auto_20150330_2335'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cidade',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='bairro',
            name='estado',
        ),
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 23, 51, 56, 140201)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='paciente',
            name='cidade',
            field=models.ForeignKey(to='usuarios.Cidade'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='paciente',
            name='estado',
            field=models.CharField(default=b'', max_length=50),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profissional',
            name='cidade',
            field=models.ForeignKey(to='usuarios.Cidade'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profissional',
            name='estado',
            field=models.CharField(default=b'', max_length=50),
            preserve_default=True,
        ),
        migrations.DeleteModel(
            name='Estado',
        ),
    ]
