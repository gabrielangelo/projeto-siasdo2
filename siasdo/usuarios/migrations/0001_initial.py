# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('area', models.CharField(max_length=50)),
                ('criado_em', models.DateTimeField(default=datetime.datetime(2015, 3, 10, 3, 29, 44, 567087))),
            ],
            options={
                'ordering': ('area',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Atendimento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.CharField(max_length=500)),
                ('create_in', models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Bairro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=100)),
                ('criado_em', models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Consulta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('msg', models.CharField(max_length=1000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Consultas_Geral',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Paciente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=75)),
                ('activation_key', models.CharField(max_length=40, blank=True)),
                ('key_expires', models.DateTimeField(default=datetime.datetime.now)),
                ('nome', models.CharField(default=b'', max_length=100)),
                ('slug', models.SlugField(unique=True, editable=False, blank=True)),
                ('cep', models.CharField(default=b'', max_length=9)),
                ('estado', models.CharField(default=b'', max_length=50)),
                ('cidade', models.CharField(default=b'', max_length=50)),
                ('endereco', models.CharField(default=b'', max_length=50)),
                ('complemento', models.CharField(default=b'', max_length=200)),
                ('criado_em', models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)),
                ('updated', models.DateTimeField(default=datetime.datetime.now, auto_now=True)),
                ('bairro', models.ForeignKey(to='usuarios.Bairro')),
                ('user', models.OneToOneField(null=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('nome',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profissional',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(default=b'', max_length=75)),
                ('activation_key', models.CharField(max_length=40, blank=True)),
                ('key_expires', models.DateTimeField(default=datetime.datetime.now)),
                ('nome', models.CharField(max_length=100)),
                ('slug', models.SlugField(unique=True, editable=False, blank=True)),
                ('cep', models.CharField(default=b'', max_length=9)),
                ('estado', models.CharField(default=b'', max_length=50)),
                ('cidade', models.CharField(default=b'', max_length=50)),
                ('endereco', models.CharField(default=b'', max_length=50)),
                ('complemento', models.CharField(default=b'', max_length=200, blank=True)),
                ('criado_em', models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)),
                ('updated', models.DateTimeField(default=datetime.datetime.now, auto_now=True)),
                ('curriculo', models.FileField(upload_to=b'siasdo/site_siasdo/static/assets/images', blank=True)),
                ('tem_certificado', models.BooleanField(default=False)),
                ('area', models.ForeignKey(to='usuarios.Area')),
                ('bairro', models.ForeignKey(to='usuarios.Bairro')),
                ('user', models.OneToOneField(null=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('nome',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rota',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bairro', models.CharField(max_length=200)),
                ('profissional', models.ForeignKey(to='usuarios.Profissional')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Servicos',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descricao', models.CharField(max_length=100)),
                ('profissional', models.ForeignKey(to='usuarios.Profissional')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='consultas_geral',
            name='paciente',
            field=models.ForeignKey(to='usuarios.Paciente'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='consultas_geral',
            name='profissional',
            field=models.ForeignKey(to='usuarios.Profissional'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='consultas_geral',
            name='servico',
            field=models.ForeignKey(to='usuarios.Consulta'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='consulta',
            name='paciente',
            field=models.ForeignKey(to='usuarios.Paciente'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='consulta',
            name='profissional',
            field=models.ForeignKey(to='usuarios.Profissional'),
            preserve_default=True,
        ),
    ]
