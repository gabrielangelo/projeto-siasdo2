# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0019_auto_20150311_2133'),
    ]

    operations = [
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 11, 22, 4, 14, 572806)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profissional',
            name='foto',
            field=models.ImageField(default=b'siasdo/site_siasdo/static/assets/images/profile_none.png', upload_to=b'media/profile-foto'),
            preserve_default=True,
        ),
    ]
