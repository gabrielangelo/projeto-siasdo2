# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0031_auto_20150328_1516'),
    ]

    operations = [
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 30, 23, 34, 48, 241036)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='paciente',
            name='estado',
            field=models.ForeignKey(to='usuarios.Estado'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profissional',
            name='estado',
            field=models.ForeignKey(to='usuarios.Estado'),
            preserve_default=True,
        ),
    ]
