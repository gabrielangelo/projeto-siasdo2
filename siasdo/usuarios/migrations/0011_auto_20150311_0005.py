# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0010_auto_20150310_0411'),
    ]

    operations = [
        migrations.AddField(
            model_name='profissional',
            name='numero',
            field=models.CharField(default=b'', max_length=10),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='area',
            name='criado_em',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 11, 0, 5, 5, 946239)),
            preserve_default=True,
        ),
    ]
