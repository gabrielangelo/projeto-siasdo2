# coding=utf8
# -*- coding: utf8 -*-
# vim: set fileencoding=utf8 :
from django.db import models
from datetime import datetime
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from PIL import Image as Img
import StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile




# Create your models here.


# Create your models here.

class Usuarios(models.Model):
	class Meta:
		abstract = True

class Paciente(Usuarios):
	class Meta:
		ordering = ('nome',)
		
	user = models.OneToOneField(User,null = True, unique = False)
	email = models.EmailField()
	activation_key =models.CharField(max_length=40, blank=True)
	key_expires = models.DateTimeField(default=datetime.now)
	
	nome = models.CharField(max_length = 100,default='')
	slug = models.SlugField(unique = True,blank = True,editable =False)
	cep = models.CharField(max_length = 9,default = '')
	estado = models.CharField(blank = True,null = True,max_length = 50,default = '')
	cidade = models.ForeignKey('Cidade')#models.CharField(max_length = 50, default = '')
	bairro =models.ForeignKey('Bairro')# models.CharField(max_length = 200)
	endereco = models.CharField(max_length = 50,default = '')
	#senha = models.CharField(max_length = 100)	
	complemento = models.CharField(max_length = 200, default = '')
	#foto = models.ImageField(upload_to = 'siasdo/usuarios/upload_files/profile_images/')
	criado_em = models.DateTimeField(auto_now_add= True,default = datetime.now)
	updated = models.DateTimeField(auto_now = True,default = datetime.now)

	def save(self,*args,**kwrags):
		if not self.id:
			self.slug = slugify(self.email)
		super(Paciente,self).save(*args,**kwrags)
	
	def __unicode__(self):
		return self.nome


class Profissional(Usuarios):
	
	class Meta:
		ordering = ('nome',)
		
	user = models.OneToOneField( User, null = True,unique=False)
	email = models.EmailField(default = '')
	activation_key =models.CharField(max_length=40, blank=True)
	key_expires = models.DateTimeField(default=datetime.now)
	
	nome = models.CharField(max_length = 100)
	slug = models.SlugField(unique = True,blank = True,editable =False)
	cep = models.CharField(max_length = 9,default = '')
	estado = models.CharField(blank = True,null = True,max_length = 50,default = '')
	cidade = models.ForeignKey('Cidade')#models.CharField(max_length = 50, default = '')
	bairro = models.ForeignKey('Bairro')
	endereco = models.CharField(max_length = 50,default = '')
	complemento = models.CharField(max_length = 200, default = '',blank = True)
	foto = models.ImageField(upload_to = 'profile-foto/', default = 'profile-none/profile_none.png' )
	area = models.ForeignKey('Area',)
	criado_em = models.DateTimeField(auto_now_add= True,default = datetime.now)
	updated = models.DateTimeField(auto_now = True,default = datetime.now)
	curriculo = models.FileField(upload_to = 'curriculos/',blank = True)
	tem_certificado = models.BooleanField(default = False)
	numero = models.CharField(max_length = 10, default = '')	
	
	def save(self,*args,**kwrags):
		if not self.id:
			self.slug = slugify(self.email)
		if self.foto:
			image = Img.open(StringIO.StringIO(self.foto.read()))
	        image.thumbnail((184,184), Img.ANTIALIAS)
	        output = StringIO.StringIO()
	        image.save(output, format='JPEG', quality=75)
	        output.seek(0)
	        self.foto= InMemoryUploadedFile(output,'ImageField', "%s.jpg" %self.foto.name, 'image/jpeg', output.len, None)
	   	super(Profissional,self).save(*args,**kwrags)
		
		 
       	 	


	def __unicode__(self):
		return self.nome


class Area(models.Model):
	class Meta:
		ordering = ('area',)
	
	
	#ide = models.AutoField(primary_key=True)
	area = models.CharField(max_length= 50)
	criado_em = models.DateTimeField(default=datetime.now())

	def __unicode__(self):
		return self.area



# class Atendimento(models.Model):
# 	message = models.CharField(max_length = 500)
# 	create_in = models.DateTimeField(auto_now_add = True, default = datetime.now)

class Rota(models.Model):
	
	bairro = models.CharField(max_length = 200)
	profissional = models.ForeignKey('Profissional')
	def __unicode__(self):	
		return self.bairro

	


class Servicos(models.Model):
	profissional = models.ForeignKey('Profissional')
	descricao = models.CharField(max_length = 100)

	def __unicode__(self):
		return self.descricao


	
class Bairro(models.Model):
	nome = models.CharField(max_length = 100)
	criado_em = models.DateTimeField(auto_now_add = True,default = datetime.now)
	cidade = models.ForeignKey('Cidade')
	def __unicode__(self):
		return self.nome

class Cidade(models.Model):
	nome = models.CharField(max_length = 100)
	def __unicode__(self):
		return self.nome

# class Consulta(models.Model):
# 	profissional = models.ForeignKey('Profissional')
# 	paciente = models.ForeignKey('Paciente')
# 	msg = models.CharField(max_length = 1000) 
# 	#status_prof = models.BooleanField(default = False)
# 	# status_paciente = models.BooleanField(default = False)
# 	# status_geral = models.BooleanField(default = False)

# class Consultas_Geral(models.Model):
# 	profissional = models.ForeignKey('Profissional')
# 	paciente = models.ForeignKey('Paciente')
# 	#id = models.AutoField(primary_key = True)
# 	servico = models.ForeignKey('Consulta')





