from django.contrib import admin
from models import  Paciente, Profissional,Area,Bairro,Cidade#Comentarios_profissionais,Comentarios_Clientes,
from django.contrib.admin.options import ModelAdmin


# Register your models here.

#class ComentarioInline_profissional(admin.TabularInline):
#	model = Comentarios_profissionais
#	extra = 1

#class ComentarioInline_Cliente(admin.TabularInline):
#	model = Comentarios_Clientes
#	extra = 1

class AdminProfessional(ModelAdmin):
	
	list_display = ('user','nome','area','email','cidade','estado','bairro','cep','foto','criado_em')
	
	search_fields = ('nome',#'email')
	)
	list_filter = ('nome',#'email')
	)
	#inlines = [ComentarioInline_profissional,]
	#prepopulated_fields = {'slug': ('area','nome',)}

class AdminArea(ModelAdmin):
	
	list_display = ('area','criado_em',)

class AdminBairro(ModelAdmin):
	list_display = ('nome','criado_em','cidade')


	


class AdminPacient(ModelAdmin):
	list_display = ('user','nome','email','cidade','estado','bairro','cep','criado_em')
	search_fields = ('nome',)
	list_filter = ('nome',)
	#prepopulated_fields = {'slug': ('nome',)}#'rua','bairro','criado_em')
	#inlines = [ComentarioInline_Cliente,]
	
#'rua','bairro','numero','foto','area_de_atuacao','criado_em',
#'area_de_atuacao'
#'rua','bairro','area_de_atuacao','criado_em'
class AdminCidade(ModelAdmin):
	list_display = ('nome',)



admin.site.register(Cidade, AdminCidade)
admin.site.register(Bairro,AdminBairro)
admin.site.register(Paciente,AdminPacient)
admin.site.register(Profissional,AdminProfessional)
admin.site.register(Area,AdminArea)


