# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consultas', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='consultaprofissional',
            name='view',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
