# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0040_auto_20150331_2329'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConsultaPaciente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('msg', models.CharField(max_length=1000)),
                ('status_consulta', models.BooleanField(default=False)),
                ('paciente', models.ForeignKey(to='usuarios.Paciente')),
                ('profissional', models.ForeignKey(to='usuarios.Profissional')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ConsultaProfissional',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('msg', models.CharField(max_length=1000)),
                ('status_consulta', models.BooleanField(default=False)),
                ('paciente', models.ForeignKey(to='usuarios.Paciente')),
                ('profissional', models.ForeignKey(to='usuarios.Profissional')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
