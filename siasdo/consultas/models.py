from django.db import models
from siasdo.usuarios.models import Profissional,Paciente
# Create your models here.


class ConsultaProfissional(models.Model):
	profissional = models.ForeignKey(Profissional)
	paciente = models.ForeignKey(Paciente)
	msg = models.CharField(max_length = 1000)
	status_consulta = models.BooleanField(default = False)
	view = models.BooleanField(default = True)

class ConsultaPaciente(models.Model):
	profissional = models.ForeignKey(Profissional)
	paciente = models.ForeignKey(Paciente)
	msg = models.CharField(max_length = 1000)
	status_consulta = models.BooleanField(default = False)


