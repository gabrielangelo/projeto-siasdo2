# coding=utf8
# -*- coding: utf8 -*-
# vim: set fileencoding=utf8 :
from django.shortcuts import render_to_response,render
from django.http import HttpResponse,HttpResponseRedirect
from siasdo.usuarios.models import Profissional,Paciente,Rota,Servicos,Bairro
from .forms import ProfissionalForm,PacienteForm,LoginForm,EditSettingFormProfissional,EditSettingFormPaciente,PasswordChangeForm,RotaForm,FiltroAreaForm,ServicosAddForm,ServicoRequest,ResetEmailForm,GeralSearchForm,ChangeFoto,Avalie
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.generic import UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.template import RequestContext
#from django.contrib.auth.views import password_change
from django.core.mail import send_mail
import hashlib, datetime, random
from django.utils import timezone
from django.http import Http404
import json as simplejson
#from django.views.generic.detail.SingleObjectMixin import get_object


# Create your views hereself.
def home(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/profile/')
	return render_to_response('index.html',{},context_instance=RequestContext(request))

#redireciona para pagina de profissional ou de paciente
@login_required
def profile(request):
	
	username = request.user
	prof = Profissional.objects.filter(user= username)
	cliente = Paciente.objects.filter(user= username)
	if prof.exists():
		return HttpResponseRedirect('/profile_profissional/')
	elif cliente.exists():
		return HttpResponseRedirect('/profile_paciente/')
	else:
		return HttpResponseRedirect('/admin/')
@login_required
def profile_redirect_search(request):
	prof = Profissional.objects.filter(user = request.user)
	paciente = Paciente.objects.filter(user = request.user)
	if prof.exists():
		return HttpResponseRedirect('/profile_profissional/search/')
	elif paciente.exists():
		return HttpResponseRedirect('/profile_paciente/search/')
	else:
		return HttpResponseRedirect('/admin/') 

##################sessao profissional junto com funcoes de mudanca de informacoes###################
#view da pagina de perfil dos profissionais
@login_required
def profile_profissional(request):
	 '''if not request.user.is_authenticated():
	 	return HttpResponseRedirect('/login/')'''
	 '''user = request.user
	 prof = Profissional.objects.get(user = user)
	 context = {'prof':prof}'''
	 prof_log = True
	 prof = Profissional.objects.get(user = request.user)
	 context = {'prof':prof,'prof_log':prof_log}
	 #flag pra profissional_profile no template index_logado
	 return render_to_response('index_logado.html',context,context_instance = RequestContext(request))
@login_required
def profile_profissional_list(request):
	user = request.user
	prof = Profissional.objects.get(user = user)
	consultas = ConsultaProfissional.objects.filter(profissional= prof,view = True)
	form = ChangeFoto()
	context = {'prof':prof,'consultas':consultas,'form':form}
	return render_to_response('profile_profissional.html',context,context_instance = RequestContext(request))
@login_required
def profile_profissional_search(request):
	if request.method =='POST':
		form =  FiltroAreaForm(request.POST or None)
		form2 = GeralSearchForm(request.POST or None)
		
		if form.is_valid():
			prof_area = Profissional.objects.filter(user__is_active = True,area = form.cleaned_data['area'],tem_certificado = True)
			area =  form.cleaned_data['area']
			flag_area = True
			flag_paciente_log = False
			form2 = GeralSearchForm()
			form = FiltroAreaForm()
			context = {'form':form,'form2':form2,'prof_area':prof_area,'flag_area':flag_area,'area':area,'flag_paciente_log':flag_paciente_log}
			
			return render_to_response('search.html',context,context_instance = RequestContext(request))
		
		elif form2.is_valid():
			search = Profissional.objects.filter(nome__icontains = form2.cleaned_data['busca'])
			form = FiltroAreaForm()
			
			form2 = GeralSearchForm()
			flag_paciente_log = False
			flag_geral = True
			context = {'search':search,'flag_geral':flag_geral,'flag_paciente_log':flag_paciente_log,'form':form,'form2':form2}

			return render_to_response('search.html',context,context_instance = RequestContext(request))

	user = request.user
	flag_paciente_log = False
	form2 = GeralSearchForm()
	bairro = Profissional.objects.get(user = user).bairro
	prof_bairro = Profissional.objects.filter(rota__bairro = bairro,user__is_active = True,tem_certificado = True)
	flag_bairro = True
	form = FiltroAreaForm()
	context = {'form2':form2,'prof_bairro':prof_bairro,'form':form,'flag_bairro':flag_bairro,'flag_paciente_log':flag_paciente_log}
	return render_to_response('search.html',context,context_instance = RequestContext(request))



#view genrica para atualizacoes de informaçoes profissionais 
@login_required
def EditSettingsProfissional(request):
	queryset =Profissional.objects.get(user = request.user)
	if request.POST:
		form = EditSettingFormProfissional(request.POST, request.FILES,instance = queryset)
		if form.is_valid():
			form.save()
			message_success = 'Dados atualizados'
			return render_to_response('config_profile_prof.html',{'form':form,'message_success':message_success},context_instance = RequestContext(request))
		return render_to_response('config_profile_prof.html',{'form':form},context_instance = RequestContext(request))
	form = EditSettingFormProfissional(instance = queryset)
	return render_to_response('config_profile_prof.html',{'form':form},context_instance = RequestContext(request))

	'''form_class = EditSettingFormProfissional
	model = Profissional
	template_name = 'config_profile_prof.html'
	success_url = '/profile_profissional/edit_settings/'
	success_message = 'dados atualizados'
	exclude = ('user','email','slug','key_expires','activation_key')


	def get_object(self):
		return Profissional.objects.get(user = self.request.user)

	def get(self,*args,**kwargs):
		self.object = Profissional.objects.get(user = self.request.user)
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		#success_message = self.get_success_message()
		context = self.get_context_data(object = self.object, form = form)
		return self.render_to_response(context)'''



##configurações de paciente
@login_required
def EditSettingsPaciente(request):
	queryset = Paciente.objects.get(user = request.user)
	if request.POST:
		form = EditSettingFormPaciente(request.POST,instance = queryset,user = request.user)
		if form.is_valid():
			form.save()
			flag_save = True
			
			return render_to_response('config_profile_paciente.html',{'form':form,'flag_save':flag_save},context_instance = 
				RequestContext(request))
	flag_save = False
	form = EditSettingFormPaciente(instance = queryset)
	return render_to_response('config_profile_paciente.html',{'form':form},context_instance = 
				RequestContext(request))


  	
## view da pagina do perfil de profissional
@login_required
def profile_paciente(request):
	user = request.user
	paciente = Paciente.objects.get(user = user)
	cep = Paciente.objects.get(user = request.user).cep
	profs = Profissional.objects.filter(cep = cep)
	context = {'paciente': paciente,'profs':profs}
	return render_to_response('index_logado.html', context, context_instance = RequestContext(request))
	

@login_required
def profile_paciente_search(request):
	if request.method =='POST':
		form =  FiltroAreaForm(request.POST or None)
		form2 = GeralSearchForm(request.POST or None)
		
		if form.is_valid():
			prof_area = Profissional.objects.filter(user__is_active = True,area = form.cleaned_data['area'],tem_certificado = True)
			area =  form.cleaned_data['area']
			flag_area = True
			flag_paciente_log = True
			form2 = GeralSearchForm()
			form = FiltroAreaForm()
			context = {'form':form,'form2':form2,'prof_area':prof_area,'flag_area':flag_area,'area':area,'flag_paciente_log':flag_paciente_log}
			
			return render_to_response('search.html',context,context_instance = RequestContext(request))
		
		elif form2.is_valid():
			search = Profissional.objects.filter(nome__icontains = form2.cleaned_data['busca'])
			form = FiltroAreaForm()
			
			form2 = GeralSearchForm()
			flag_paciente_log = True
			flag_geral = True
			context = {'search':search,'flag_geral':flag_geral,'flag_paciente_log':flag_paciente_log,'form':form,'form2':form2}

			return render_to_response('search.html',context,context_instance = RequestContext(request))

	# form = FiltroAreaForm(request.POST or None)
	# if request.method == 'POST' and not form.is_valid():
	# 	if form2.is_valid():
			# form = FiltroAreaForm()
			# form2 = GeralSearchForm()
			# search = Profissional.objects.filter(nome__icontains = form2.cleaned_data['busca'])
			# flag_paciente_log = True
			# flag_geral = True
			# context = {'search':search,'flag_geral':flag_geral,'flag_paciente_log':flag_paciente_log,'form':form,'form2':form2}

			# return render_to_response('search.html',context,context_instance = RequestContext(request))

	user = request.user
	flag_paciente_log = True
	form2 = GeralSearchForm()
	bairro = Paciente.objects.get(user = user).bairro
	prof_bairro = Profissional.objects.filter(rota__bairro = bairro,user__is_active = True,tem_certificado = True)
	flag_bairro = True
	form = FiltroAreaForm()
	context = {'form2':form2,'prof_bairro':prof_bairro,'form':form,'flag_bairro':flag_bairro,'flag_paciente_log':flag_paciente_log}
	return render_to_response('search.html',context,context_instance = RequestContext(request))




##################sessao profissional junto com funcoes de mudanca de informacoes########################

##views para formularios de cadastros de profissionais e pacientes
def profissionais(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/profile/')
	if request.method == 'POST':
		form =ProfissionalForm(request.POST or None, request.FILES)
		if form.is_valid():
			user = User.objects.create_user(username = form.cleaned_data['email'],password = 
			form.cleaned_data['senha'],email = form.cleaned_data['email']
			)
			user.is_active = False
			user.save()
			
			
			salt = hashlib.sha1(str(random.random())).hexdigest()[:5] 
			activation_key = hashlib.sha1(salt+form.cleaned_data['email']).hexdigest()
			key_expires = datetime.datetime.today() + datetime.timedelta(2)
			
			foto = None
			foto = form.cleaned_data['foto']

			if foto is not None:
				
				profissional = Profissional(user=user,nome =form.cleaned_data['nome'],cep = form.cleaned_data['cep'],email =
					form.cleaned_data['email']
					,cidade = form.cleaned_data['cidade'],bairro = form.cleaned_data['bairro'],
					endereco = form.cleaned_data['endereco'], complemento = form.cleaned_data['complemento'],area = 
				form.cleaned_data['area'],activation_key = activation_key,key_expires = key_expires,foto = foto,
					curriculo = form.cleaned_data['curriculo'],numero = form.cleaned_data['numero'])
				profissional.save()
				auth = authenticate(username = form.cleaned_data['email'], password = form.cleaned_data['senha'])
			
			else:
				profissional = Profissional(user=user,nome =form.cleaned_data['nome'],cep = form.cleaned_data['cep'],email =
				form.cleaned_data['email'],
				cidade = form.cleaned_data['cidade'],bairro = form.cleaned_data['bairro'],
				endereco = form.cleaned_data['endereco'], complemento = form.cleaned_data['complemento'],area = 
				form.cleaned_data['area'],activation_key = activation_key,key_expires = key_expires,
				curriculo = form.cleaned_data['curriculo'],numero = form.cleaned_data['numero'])
				profissional.save()

				#enviar email de confirmação
			username = form.cleaned_data['nome']
			email_subject = u"confirmação de email"
			email_body = "Hey %s, agradecemos pelo seu cadastro! Para ativar sua conta, clique no link abaixo em ate 48 horas/ http://siasdo.com.br/accounts/confirm/%s" % (username, activation_key)#thanks for signing up. To activate your account, click this link within 48hours\ http://127.0.0.1:8000/accounts/confirm/%s" % (username, activation_key)
			send_mail(email_subject, email_body, 'siasdoequipe@gmail.com',[form.cleaned_data['email']], fail_silently=False)
           	#return HttpResponseRedirect('/register/success_register/')
				#'''auth = authenticate(username = form.cleaned_data['email'], password = form.cleaned_data['senha'])
				#if auth is not None:
					#login(request,auth)
			return HttpResponseRedirect('/register/success_register/')

			
			
		else:
			return render_to_response('profissionais.html',{'form':form},context_instance=RequestContext(request))
	else:
		form = ProfissionalForm()
		#template = 'profissionais.html'
		return render_to_response('profissionais.html',{'form':form},context_instance=RequestContext(request))


def pacientes(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/profile/')
	if request.method == 'POST':
		form =  PacienteForm(request.POST or None )
		if form.is_valid():
			user = User.objects.create_user(username = form.cleaned_data['email'], password = form.cleaned_data['senha'],
				email = form.cleaned_data['email'])
			user.is_active = False
			user.save()
			
			salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
			activation_key = hashlib.sha1(salt+form.cleaned_data['email']).hexdigest()
			key_expires = datetime.datetime.today() + datetime.timedelta(2)
			
			paciente = Paciente(user = user,nome =form.cleaned_data['nome'],cep = form.cleaned_data['cep'],email = 
				form.cleaned_data['email'],
				cidade = form.cleaned_data['cidade'],bairro = form.cleaned_data['bairro'],
				endereco = form.cleaned_data['endereco'], complemento = form.cleaned_data['complemento']
				,activation_key = activation_key,key_expires = key_expires)
			paciente.save()
			
			#auth = authenticate(username = form.cleaned_data['email'],password = form.cleaned_data['senha'])
			username = form.cleaned_data['nome']
			email_subject = u"confirmação de email"
			email_body = "Hey %s, agradecemos pelo seu cadastro! Para ativar sua conta, clique no link abaixo em ate 48 horas/ http://siasdo.com.br/accounts/confirm/%s" % (username, activation_key)

			send_mail(email_subject, email_body, 'siasdoequipe@gmail.com',[form.cleaned_data['email']], fail_silently=False)
			return HttpResponseRedirect('/register/success_register/')
			#if auth is not None:g
				#login(request,auth)
				#return HttpResponseRedirect('/profile_paciente/')
		else:
			return render_to_response('paciente.html',{'form':form},context_instance=RequestContext(request))
	else:
		form = PacienteForm()
		return render_to_response('paciente.html',{'form':form},context_instance= RequestContext(request))


# View de Login

def LoginRequest(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/profile/')
	if request.method == 'POST':
		form = LoginForm(request.POST or None)
		if form.is_valid():	
			username = form.cleaned_data['email']
			password = form.cleaned_data['senha']
			if User.objects.get(username = username).is_active == False: 
				#'''login(request,form.get_user())
				#request.session['member_id'] = User.objects.get(username = username).id 
				#return HttpResponseRedirect('/profile/')'''
				message_active = 'Você estar inativo no momento, por favor confirme seu email'
				flag_user = True
				context = {'form':form,'flag_user':flag_user,'message_active':message_active}
				return render_to_response('login.html',context,context_instance = RequestContext(request))
			if not request.POST.get('remember_me', None):
				request.session.set_expiry(0)
			login(request,form.get_user())
			#request.session['member_id'] = User.objects.get(username = username).id
			
			return HttpResponseRedirect('/profile/')

				
		else:
			
			return render_to_response('login.html',{'form':form},context_instance= RequestContext(request))
	
	else:
		form = LoginForm()
		return render_to_response('login.html',{'form':form},context_instance = RequestContext(request))


def register_confirm_email(request,activation_key):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/')
	
	
	profile_prof = Profissional.objects.filter(activation_key = activation_key)
	if profile_prof.exists():
		if profile_prof.get().key_expires<timezone.now():
			return HttpResponseRedirect('error_page/')
		else:
			user = profile_prof.get().user	
			user.is_active = True
			user.save()
			return render_to_response('succes_confirm_email.html',{},context_instance = RequestContext(request))
	

		 	
	
	profile_paciente = Paciente.objects.filter(activation_key = activation_key)
	if profile_paciente.exists():
		if profile_paciente.get().key_expires<timezone.now():
			return HttpResponseRedirect('error_page/')

		else:
			user = profile_paciente.get().user
			user.is_active = True
			user.save()
			return render_to_response('succes_confirm_email.html',{},context_instance = RequestContext(request))
	
	


@login_required
def search_page(request):   
	profs = Profissional.objects.all()
	med = Profissional.objects.filter(area = 3).count()
	fisio = Profissional.objects.filter(area = 2).count()
	context = {'profs':profs,'med':med,'fisio':fisio}            
	
	return render_to_response('search.html',context,context_instance =RequestContext(request))

def LogoutRequest(request):
    logout(request)
    return HttpResponseRedirect('/')


@login_required
def avalie(request):
	if request.POST:
		form = Avalie(request.POST)
		if form.is_valid():
			user = None
			categoria = None
			email = 'siasdoavaliacao@gmail.com'
			msg = form.cleaned_data['msg']
			nota = form.cleaned_data['nota']
			prof = Profissional.objects.filter(user = request.user)
			paciente = Paciente.objects.filter(user = request.user)
			if prof.exists():
				user = prof.get().nome
				categoria = 'profissional'
			elif paciente.exists():
				user = paciente.get().nome
				categoria = 'paciente'

			email_subject = u"O %s %s enviou uma avaliação do site"%(categoria,user)
			email_body = u'nota: %s\nSugestão: %s\n' % (nota,msg)#thanks for signing up. To activate your account, click this link within 48hours\ http://127.0.0.1:8000/accounts/confirm/%s" % (username, activation_key)
			send_mail(email_subject, email_body, 'siasdoavaliacao@gmail.com',[email], fail_silently=False)
			
			
			message_success = 'Mensagem enviada, obrigado por contribuir para o nosso crescimento'
			context = {'form':form,'message_success':message_success}
			return render_to_response('avalie.html',context,context_instance = RequestContext(request))
	
	form = Avalie()
	context = {'form':form}
	return render_to_response('avalie.html',context,context_instance = RequestContext(request))


def search(request):
	return render_to_response('search.html',context_instance = RequestContext(request))

def password(request):
	return render_to_response('forgot_password.html', context_instance = RequestContext(request))

def success(request):
	return render_to_response('success.html', context_instance = RequestContext(request))

def config_profile(request):
	return render_to_response('config_profile.html', context_instance = RequestContext(request))

def message(request):
	return render_to_response('message.html', context_instance = RequestContext(request))

def alterar_senha(request):
	return	render_to_response('alterar_senha.html', context_instance = RequestContext(request))

from siasdo.consultas.models import ConsultaProfissional,ConsultaPaciente

@login_required
def single_profissional(request,slugprof):
	if not Profissional.objects.filter(slug = slugprof,tem_certificado = True).exists():
		return HttpResponseRedirect('/profile_paciente/search/')
	if request.POST:
		prof = Profissional.objects.get(slug = slugprof)
		servicos = Servicos.objects.filter(profissional = prof)
		
		form = ServicoRequest(request.POST)
		if form.is_valid():
			# consulta_perm = Consulta(msg = form.cleaned_data['msg'],profissional = prof,paciente = Paciente.objects.get(user=request.user))
			# consulta_perm.save()
			# consulta_dynamic = Consultas_Geral(servico = consulta_perm,profissional = prof,paciente = Paciente.objects.get(user=request.user))
			# consulta_dynamic.save()
			consulta_prof = ConsultaProfissional(msg = form.cleaned_data['msg'],profissional = prof,
				paciente = Paciente.objects.get(user=request.user))
			consulta_prof.save()
			consulta_paciente = ConsultaPaciente(msg = form.cleaned_data['msg'],profissional = prof,paciente = Paciente.objects.get(user=request.user))
			consulta_paciente.save()
			
			email_subject = u"Voc tem uma nova consulta Siasdo!"
			email_body = u"Hey %s, novas pessoas estão interessadas nos seus serviços!" % (prof.nome)
			send_mail(email_subject, email_body, 'siasdoequipe@gmail.com',[prof.email], fail_silently=False)
			message_success = 'Sua solicitação de atendimento foi solicitada com sucesso.'
			
			if not Profissional.objects.filter(user = request.user).exists():
				prof = Profissional.objects.get(slug = slugprof)
				form = ServicoRequest()
				curriculo = prof.curriculo
				if len(curriculo.name) == 0:
					flag_cur = False

					return render_to_response('message.html',{'message_success':message_success,'prof':prof,'flag_cur':flag_cur,'form':form,'servicos':servicos},context_instance = RequestContext(request))
				flag_cur = True
				return render_to_response('message.html',{'message_success':message_success,'prof':prof,'flag_cur':flag_cur,'form':form,'servicos':servicos},context_instance = RequestContext(request))
		
			prof_not_log = True
			
			form = ServicoRequest()
			message_not_log = u'Você precisa estar logado como paciente para contatar profissionais'
			context = {'prof_not_log':prof_not_log,'message_not_log':message_not_log}
			return render_to_response('error_page.html',context,context_instance = RequestContext(request))
	
	if not Profissional.objects.filter(user = request.user).exists():
		prof = Profissional.objects.get(slug = slugprof)
		servicos = Servicos.objects.filter(profissional = prof)
		curriculo = prof.curriculo
		form = ServicoRequest()
		if len(curriculo.name) == 0:
			flag_cur = False
			return render_to_response('message.html',{'prof':prof,'flag_cur':flag_cur,'form':form,'servicos':servicos},context_instance = RequestContext(request))
		flag_cur = True
		return render_to_response('message.html',{'prof':prof,'flag_cur':flag_cur,'form':form,'servicos':servicos},context_instance = RequestContext(request))
	
	prof_not_log = True
	message_not_log = u'Você precisa estar logado como paciente para contatar profissionais'
	context = {'prof_not_log':prof_not_log,'message_not_log':message_not_log}
	return render_to_response('error_page.html',context,context_instance = RequestContext(request))



@login_required
def add_rote(request):
	if request.method == 'POST':
		
		form = RotaForm(request.POST,user = request.user)
		if form.is_valid():
			profissional = Profissional.objects.get(user = request.user)
			rota = Rota(bairro = form.cleaned_data['bairro'])
			profissional.rota_set.add(rota)
			message = 'rota adicionada!'
			rotas = Rota.objects.filter(profissional = profissional).order_by('bairro')
			context = {'form':form,'message':message,'rotas':rotas,'success':success}
			return render_to_response('add_rota.html',context,context_instance = RequestContext(request))
		else:
			profissional = Profissional.objects.get(user = request.user)
			message_error = 'Rota com bairro ja existente!'
			form1 = RotaForm(user = request.user)
			rotas = Rota.objects.filter(profissional = profissional).order_by('bairro')
			context = {'message_error':message_error,'form1':form1,'rotas':rotas}
			return render_to_response('add_rota.html',context,context_instance = RequestContext(request))

	else:
		#bairro_id = Profissional.objects.get(user = request.user).cidade.id
		form = RotaForm(user = request.user)
		profissional = Profissional.objects.get(user = request.user)
		rotas = Rota.objects.filter(profissional = profissional).order_by('bairro')
		return render_to_response('add_rota.html',{'form':form,'rotas':rotas},context_instance = RequestContext(request))



			 
			  
			 
@login_required			  
def delete_rote(request,id_rote):
	rota = Rota.objects.filter(pk = id_rote)
	rota.delete()
	return HttpResponseRedirect('/profile_profissional/add_rota/')

@login_required
def delete_rote_page(request):
	profissional = Profissional.objects.get(user = request.user)
	rotas = Rota.objects.filter(profissional = profissional)
	message = 'rota removida com sucesso!'
	context = {'rotas':rotas,'message':message}
	return render_to_response('delete_rote.html',context,context_instance = RequestContext(request))

def error_page(request):
	return render_to_response('error_page.html', context_instance = RequestContext(request))

def index_logado(request):
	return render_to_response('index_logado.html', context_instance = RequestContext(request))


def success_register(request):
	return render_to_response('success_register.html',{},context_instance = RequestContext(request))
'''def search(request):
	if request.method =='POST':
		form =  FiltroAreaForm(request.POST)
		if form.is_valid():
			user = request.user
			paciente = Paciente.objects.get(user = user)
			cep = Paciente.objects.get(user = request.user).cep
			prof_area = Profissional.objects.filter(area = form.cleaned_data['area'])
			context = {'prof_area':prof_area,'form':form}
			return render_to_response('search.html',context,context_instance = RequestContext(request))
			
	prof_cep = Profissional.objects.filter(cep = cep)
	form = FiltroAreaForm()
	context = {'prof_area':prof_area,'form':form}
	return render_to_response('search.html',context,context_instance = RequestContext(request))'''
@login_required
def success_config_profile(request):
	return render_to_response('success_config_profile.html',{},context_instance = RequestContext(request))


def changephoto(request):
	form = Changephoto()
	return render_to_response('changephoto.html',{'form':form},context_instance = RequestContext(request))

@login_required
def get_curriculo(request,idprof):
	prof =  Profissional.objects.get(id = idprof)
	curriculo =prof.curriculo.url
	context = {'curriculo':curriculo,'prof':prof}
	return render_to_response('curriculo.html',context,context_instance = RequestContext(request))
		
'''def err_404(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/')
	flag_expired = True
	message_expired = 'Link expirado!'
	return render_to_response('expired_email.html',{'flag_expired':flag_expired,
		'message_expired':message_expired},context_instance = RequestContext(request))'''



@login_required
def add_servico(request):
	if request.POST:
		form = ServicosAddForm(request.POST,user = request.user)
		if form.is_valid():
			profissional = Profissional.objects.get(user = request.user)
			servico = Servicos(descricao = form.cleaned_data['descricao'])
			profissional.servicos_set.add(servico)
			message = 'Serviço adicionado!'
			servicos = Servicos.objects.filter(profissional =profissional).order_by('descricao')
			context = {'message':message,'form':form,'servicos':servicos}
			return render_to_response('add_servico.html',context,context_instance =RequestContext(request))
		else:
			profissional = Profissional.objects.get(user = request.user)
			servicos = Servicos.objects.filter(profissional =profissional).order_by('descricao')
			message_error = 'Serviço já existente!'
			context = {'message_error':message_error,'form':form,'servicos':servicos}
			return render_to_response('add_servico.html',context,context_instance =RequestContext(request))
	else:
		profissional = Profissional.objects.get(user =request.user)
		form = ServicosAddForm()
		servicos = Servicos.objects.filter(profissional =profissional).order_by('descricao')
		context = {'form':form,'servicos':servicos,}
		return render_to_response('add_servico.html',context, context_instance = RequestContext(request))

@login_required
def delete_servico(request,pk_servico):
	servico = Servicos.objects.filter(pk = pk_servico)
	servico.delete()
	return HttpResponseRedirect('/profile_profissional/add_servico/')

@login_required
def minhas_consultas(request):
	paciente = Paciente.objects.get(user = request.user)
	consultas = ConsultaPaciente.objects.filter(paciente = paciente)
	return render_to_response('minhas_consultas.html',{'consultas':consultas},context_instance = RequestContext(request))
@login_required
def delete_consultas_profissional(request,id_consulta):
	consulta = ConsultaProfissional.objects.get(id = id_consulta)
	consulta.delete()
	return HttpResponseRedirect('/profile_profissional/persons/')

@login_required
def delete_consultas_paciente(request,id_consulta):
	consulta = ConsultaPaciente.objects.get(id = id_consulta)
	consulta.delete()
	return HttpResponseRedirect('/profile_paciente/minhas-consultas/')	

@login_required
def confirmar_consulta(request,id_consulta):
	status_consulta = True
	consulta_prof = ConsultaProfissional.objects.get(id = id_consulta)
	consulta_paciente = ConsultaPaciente.objects.get(id = id_consulta)
	setattr(consulta_prof,'status_consulta',status_consulta)
	setattr(consulta_prof,'view',True)
	setattr(consulta_paciente,'status_consulta',status_consulta)
	consulta_prof.save()
	consulta_paciente.save()
	
	profissional = ConsultaProfissional.objects.get(id = id_consulta).profissional
	paciente = ConsultaProfissional.objects.get(id = id_consulta).paciente

	
	email_subject = u"Solicitação de atendimento confirmada"
	email_body = u"Hey %s, o profissional %s confirmou sua solicitação!" % (paciente.nome,profissional.nome)
	send_mail(email_subject, email_body, 'siasdoequipe@gmail.com',[paciente.email], fail_silently=False)
	
			

	return HttpResponseRedirect('/profile_profissional/persons/')




# def login_problem(request):
# 	form = 
# 	return render_to_response('loginproblem.html',{},context_instance = RequestContext(request))
def resend_email(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/profile/')
	if request.POST:
		form = ResetEmailForm(request.POST)
		if form.is_valid():
			salt = hashlib.sha1(str(random.random())).hexdigest()[:5] 
			activation_key = hashlib.sha1(salt+form.cleaned_data['email']).hexdigest()
			key_expires = datetime.datetime.today() + datetime.timedelta(2)
			
			# user = User.objects.get(username = request.user)
			prof = Profissional.objects.filter(email = form.cleaned_data['email'])
			if prof.exists():	
				profissional = prof.get()
				setattr(profissional,'activation_key',activation_key)
				setattr(profissional,'key_expires',key_expires)
				profissional.save()

				email_subject = u"confirmação de email"
				email_body = "Hey %s, agradecemos pelo seu cadastro! Para ativar sua conta, clique no link abaixo em ate 48 horas/ http://127.0.0.1:8000/accounts/confirm/%s" % (profissional.nome, activation_key)#thanks for signing up. To activate your account, click this link within 48hours\ http://127.0.0.1:8000/accounts/confirm/%s" % (username, activation_key)
				send_mail(email_subject, email_body, 'siasdoequipe@gmail.com',[form.cleaned_data['email']], fail_silently=False)
				
				message_success = 'email enviado com sucesso!'	
				form = ResetEmailForm()
				context = {'form':form,'message_success':message_success}
				return render_to_response('resend_email.html',context,context_instance = RequestContext(request))
		
			paci = Paciente.objects.filter(email = form.cleaned_data['email'])
			if paci.exists():
				paciente = paci.get()
				setattr(paciente,'activation_key',activation_key)
				setattr(paciente,'key_expires',key_expires)
				paciente.save()

				email_subject = u"confirmação de email"
				email_body = "Hey %s, agradecemos pelo seu cadastro! Para ativar sua conta, clique no link abaixo em ate 48 horas/ http://127.0.0.1:8000/accounts/confirm/%s" % (paciente.nome, activation_key)#thanks for signing up. To activate your account, click this link within 48hours\ http://127.0.0.1:8000/accounts/confirm/%s" % (username, activation_key)
				send_mail(email_subject, email_body, 'siasdoequipe@gmail.com',[form.cleaned_data['email']], fail_silently=False)
				
				message_success = 'email enviado com sucesso!'	
				form = ResetEmailForm()
				context = {'form':form,'message_success':message_success}
				return render_to_response('resend_email.html',context,context_instance = RequestContext(request))

		context = {'form':form}
		return render_to_response('resend_email.html',context,context_instance = RequestContext(request))
	
	form = ResetEmailForm()
	context = {'form':form}
	return render_to_response('resend_email.html',context,context_instance = RequestContext(request))			

# @login_required
# def general_search(request):
# 	if request.POST:
# 		form = GeralSearchForm(request.POST)
# 		if form.is_valid():
# 			search = Profissional.objects.filter(nome__icontains = form.cleaned_data['busca'])
# 			flag_geral = True
# 			flag_paciente_log = True
# 			context = {'search':search,'flag_geral':flag_geral,'flag_paciente_log':flag_paciente_log}
# 			return render_to_response('search.html',context,context_instance = RequestContext(request))


def get_bairros(request):
	cidade = request.GET.get('cidade')
	if cidade:
		bairros = Bairro.objects.filter(cidade = cidade)
	else:
		bairros = Bairro.objects.all()
	data = []
	for b in bairros:
		data.append({'id_bairro':b.id, 'unicode':b.nome})
	json = simplejson.dumps(data)
	return HttpResponse(json,content_type='application/json; charset=utf-8')


# def remove_foto(request,id_prof):
# 	prof = Profissional.objects.get(id = id_prof)
# 	foto = prof.foto._del_file()
# 	url_default = 'profile-none/profile_none.png'
# 	setattr(prof,'foto',url)
# 	prof.save()

def cadastre_se(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/profile/')
	return render_to_response('cadastre_se.html',{},context_instance = RequestContext(request))

@login_required
def move_consultas(request,id_consulta):
	consulta = ConsultaProfissional.objects.get(id = id_consulta)
	status = False
	setattr(consulta,'view',status)
	consulta.save()
	return HttpResponseRedirect('/profile_profissional/persons/')

@login_required
def consultas_profissional_nao_confirmadas(request):
	prof = Profissional.objects.get(user = request.user)
	consultas = ConsultaProfissional.objects.filter(profissional = prof,status_consulta = False,view = False)
	context = {'consultas':consultas}
	return render_to_response('consultas_profissional_nao_confirmadas.html',context,context_instance = RequestContext(request))

@login_required
def change_foto(request):
	if request.POST:
		form = ChangeFoto(request.POST,request.FILES)
		if form.is_valid():
			new_foto = form.cleaned_data['foto']
			user = request.user
			prof = Profissional.objects.get(user = user)
			setattr(prof,'foto',new_foto)
			prof.save()
			return HttpResponseRedirect('/profile_profissional/persons/')
