# coding=utf8
# -*- coding: utf8 -*-
# vim: set fileencoding=utf8 :
from django import forms
from django.forms import ModelForm	
from siasdo.usuarios.models import Profissional,Paciente,Area,Rota,Bairro,Servicos,Cidade
from django.contrib.auth.models import User
from django.utils.translation import ugettext, ugettext_lazy as _
from collections import OrderedDict
from django.contrib.auth import authenticate
from django.contrib.auth.forms import SetPasswordForm,PasswordResetForm
from django.contrib import messages

class ProfissionalForm(ModelForm):
	
	class Meta: 
		model = Profissional
		exclude = ('user','activation_key','key_expires')
		fields = ['nome','email','senha','senha1','area','cep','cidade','bairro','endereco'
		,'complemento','foto',]
		
	
	nome = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label="*Nome:")
	#username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label = '*Usuario')
	email = forms.EmailField(widget=forms.TextInput(attrs={'class':'form-control','required':'true','type':'email'}),label="*Email:")
	area = forms.ModelChoiceField( widget=forms.Select(attrs = {'required':'true'}),queryset = Area.objects.all(),label="*Area:")
	cep = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true','OnkeyPress':'formatar("#####-###", this)','maxlength':'9'}))
	#estado = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true','maxlength':'2'}),label="*Estado(sigla)")
	cidade = forms.ModelChoiceField( widget=forms.Select(attrs = {'required':'true'}),queryset = Cidade.objects.all(),label="*Cidade:") #forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label="*Cidade")
	endereco = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label="*Endereço")
	#bairro = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label="*Bairro:")
	bairro = forms.ModelChoiceField(widget=forms.Select(attrs = {'required':'true'}),queryset = Bairro.objects.all(),label="*Bairro:")
	complemento = forms.CharField(widget = forms.Textarea(attrs={'class':'form-control'}),label="Complemento:", required = False)
	foto = forms.ImageField(label="Foto(Opcional)", required = False)
	senha = forms.CharField(widget = forms.PasswordInput(attrs={'class':'form-control','required':'true'}),label="*Senha")
	senha1 = forms.CharField(widget = forms.PasswordInput(attrs={'class':'form-control','required':'true'}),label="*Confirme a senha", required = False)
	numero = forms.CharField(widget = forms.TextInput(attrs={'class':'form-control','required':'true'}),label = "*Telefone celular")
	curriculo = forms.Field(widget =forms.FileInput(attrs = {'accept':'application/pdf'}),required = False,label = "Curriculo\n(Somente pdf)")	
	def clean_email(self):
		try:
			user = User.objects.get(username = self.cleaned_data['email'])
		except User.DoesNotExist:
			return self.cleaned_data['email']
		raise forms.ValidationError('Email já cadastrado, escolha outro')



	def clean_senha1(self):
		if self.cleaned_data['senha'] != self.cleaned_data['senha1']:
			raise forms.ValidationError('As senhas nao correspondem!')
		elif len(self.cleaned_data['senha1'])<6:
			raise forms.ValidationError('Senha deve conter no mínimo 6 caracteres')
		return self.cleaned_data['senha']
		
	def foto_url(self):
		if self.foto and hasattr(self.cleaned_data['foto'], 'url'):
			return self.cleaned_data['foto']
		else:
			return 'siasdo/usuarios/upload_files/profile_none/images.jpeg'
	def clean_numero(self):
		numero = self.cleaned_data['numero']
		try:
			if long(numero) and not numero.isalpha():
				min_lenght = 7
				max_lenght = 11
				ph_lenght = str(numero)
				if len(ph_lenght)<min_lenght or len(ph_lenght)>max_lenght:
					raise forms.ValidationError('Tamanho inválido!')
		except(ValueError,TypeError):
			raise forms.ValidationError('digite um número válido!')
		return numero
	import os

	
	def clean_curriculo(self):
		curriculo = self.cleaned_data['curriculo']
		types = ['pdf']
		content_type = str(curriculo).split('.')[-1]
		if curriculo != None:
			if content_type in types:
				if curriculo._size > 2621440:
					raise forms.ValidationError('Tamanho acima do permitido')
				return curriculo
			raise forms.ValidationError('Formato inválido, somente pdf')

	def clean_foto(self):
		foto = self.cleaned_data['foto']
		types = ['jpeg','jpg','png','gif']
		content_type = str(foto).split('.')[-1]
		if foto != None:
			if content_type in types:
				if foto._size > 5242880:
					raise forms.ValidationError('tamanho acima do permitido')
				return foto
			raise forms.ValidationError('Formatos aceitos : "jpg", "jpeg", "png", "gif"')
	
	def clean_cep(self):
		import re
		pattern1 = r'\d{5}-\d{3}'
		cep = self.cleaned_data['cep']
		if re.match(pattern1,str(cep)) is None:
			raise forms.ValidationError('formato de cep inválido')
		return cep

		

class PacienteForm(ModelForm)	:
	class Meta:	
		model = Paciente
		exclude = ('user','slug','activation_key','key_expires','foto')
		fields = ['nome','email','senha','senha1','cep','cidade','bairro','endereco','complemento']
	#username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label = '*Usuario') 
	nome = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label="*Nome:")
	email = forms.EmailField(widget=forms.TextInput(attrs={'class':'form-control','required':'true','type':'email'}),label="*Email:")
	cep = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true','OnkeyPress':'formatar("#####-###", this)', 'maxlength':'9'}),label="*Cep")
	#estado = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label="*Estado")
	cidade = forms.ModelChoiceField( widget=forms.Select(attrs = {'required':'true'}),queryset = Cidade.objects.all(),label="*Cidade:")#forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label="*Cidade")
	endereco = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label="*Endereço")
	#bairro = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':'true'}),label="*Bairro:")
	bairro = forms.ModelChoiceField(widget=forms.Select(attrs = {'required':'true'}),queryset = Bairro.objects.all(),label="*Bairro:")#forms.ModelChoiceField( widget=forms.Select(attrs = {'required':'true'}),queryset = Bairro.objects.all(),label="*Bairro:")
	complemento = forms.CharField(widget = forms.Textarea(attrs={'class':'form-control'}),label="Complemento:",required = False)
	senha = forms.CharField(widget = forms.PasswordInput(attrs={'class':'form-control','required':'true'}),label="*Senha")
	senha1 = forms.CharField(widget = forms.PasswordInput(attrs = {'class':'form-control','required':'true'}),label = "*Confirme sua Senha")

	def clean_email(self):
		try:
			user = User.objects.get(username = self.cleaned_data['email'])
		except User.DoesNotExist:
			return self.cleaned_data['email']
		raise forms.ValidationError('Email já cadastrado, escolha')
		


	def clean_senha1(self):
		if self.cleaned_data['senha'] != self.cleaned_data['senha1']:
			raise forms.ValidationError('As senhas nao correspondem!')
		elif len(self.cleaned_data['senha1'])<6:
			raise forms.ValidationError('Senha deve conter no mínimo 6 caracteres')
		return self.cleaned_data['senha']
	def clean_cep(self):
		import re
		pattern1 = r'\d{5}-\d{3}'
		cep = self.cleaned_data['cep']
		if re.match(pattern1,str(cep)) is None:
			raise forms.ValidationError('formato de cep inválido')
		return cep




class LoginForm(forms.Form):
	email = forms.EmailField(widget=forms.TextInput(attrs={'class':'form-control','required':'true','type':'email','placeholder':'Email'}),label="*Email:")
	senha = forms.CharField(widget = forms.PasswordInput(attrs={'class':'form-control','required':'true','placeholder':'Senha'}),label="*Senha")
	remember_me = forms.BooleanField(required=False, widget=forms.CheckboxInput(),label = "Lembre de mim")
	def __init__(self, *args, **kwargs):
		self.auth = None
		super(LoginForm,self).__init__(*args,**kwargs)


	def clean(self):
		username = self.cleaned_data['email']
		password = self.cleaned_data['senha']
		self.auth = authenticate(username = username,password = password)
		if self.auth is None:
			raise forms.ValidationError('email ou senha invalidos')
		
	
	def get_user(self):
		return self.auth
	

	

#falta validar o formulario
class EditSettingFormProfissional(ModelForm):
	class Meta:
		model = Profissional
		exclude = ('user','email','slug','key_expires','activation_key','foto','tem_certificado','curriculo',)

	def __init__(self,*args,**kwargs):
		self.user = kwargs.pop('user', None)
		super(EditSettingFormProfissional,self).__init__(*args,**kwargs)

	nome = forms.CharField(widget = forms.TextInput(attrs ={'class':'form-control','placeholder':'Nome','required':'true'}))
	cep = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Cep','required':'true','OnkeyPress':'formatar("#####-###", this)', 'maxlength':'9'}))
	#estado = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Estado','required':'true'}))
	cidade = forms.ModelChoiceField( widget=forms.Select(attrs = {'class':'form-control','placeholder':'Endereco','required':'true'}),queryset = Cidade.objects.all(),label="*Cidade:")#forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Cidade','required':'true'}))
	endereco = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Endereco','required':'true'}))
	bairro = forms.ModelChoiceField(widget=forms.Select(attrs = {'class':'form-control','required':'true'}),queryset = Bairro.objects.all())
	complemento = forms.CharField(widget = forms.Textarea(attrs={'class':'form-control','placeholder':'Complemento'}),required = False)
	#foto = forms.ImageField(required = False)
	area = area = forms.ModelChoiceField(widget=forms.Select(attrs = {'class':'form-control','required':'false'}),queryset = Area.objects.all(),required = False)
	#curriculo = forms.FileField(required = False)
	numero = forms.CharField(widget = forms.TextInput(attrs={'class':'form-control','required':'true'}),label = "*Número/whatsapp")
	
	# def __init__(self,*args,**kwargs):
	# 	super(EditSettingFormProfissional,self).__init__(*args,**kwargs)
	# 	if kwargs.has_key('instance'):
	# 		if kwargs['instance'].cidade_id != 0  and kwargs['instance'].cidade_id != None:
	# 			bairros = Bairro.objects.filter(cidade = kwargs['instance'].cidade_id)
	# 			for bairro in bairros:
	# 				bairr = (bairro.id,bairro.nome)
	# 			self.fields['bairro'].choices = bairr

	def clean_numero(self):
		numero = self.cleaned_data['numero']
		try:
			if long(numero) and not numero.isalpha(): 
				min_lenght = 7
				max_lenght = 11
				ph_lenght = str(numero)
				if len(ph_lenght)<min_lenght or len(ph_lenght)>max_lenght:
					raise forms.ValidationError('Tamanho inválido!')
		except(ValueError,TypeError):
			raise forms.ValidationError('digite um número válido!')
		return numero

	
class EditSettingFormPaciente(ModelForm):
	class Meta:
		model = Paciente
		exclude = ('user','email','slug','key_expires','activation_key')

	def __init__(self,*args,**kwargs):
		self.user = kwargs.pop('user',None)
		super(EditSettingFormPaciente,self).__init__(*args,**kwargs)

	nome = forms.CharField(widget = forms.TextInput(attrs ={'class':'form-control','placeholder':'Nome','required':'true'}))
	cep = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Cep','required':'true','OnkeyPress':'formatar("#####-###", this)', 'maxlength':'9'}))
	#estado = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Estado','required':'true'}))
	cidade = forms.ModelChoiceField( widget=forms.Select(attrs = {'class':'form-control','placeholder':'Estado','required':'true'}),queryset = Cidade.objects.all(),label="*Cidade:")#forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Cidade','required':'true'}))
	endereco = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Endereco','required':'true'}))
	bairro =   forms.ModelChoiceField(widget=forms.Select(attrs = {'class':'form-control','required':'true'}),queryset = Bairro.objects.all())
	complemento = forms.CharField(widget = forms.Textarea(attrs={'class':'form-control','placeholder':'Complemento'}),required = False)

	'''def save1(self):
		pac = Paciente.objects.get(user = self.user)
		pac.nome = self.cleaned_data['nome']
		pac.cep = self.cleaned_data['cep']
		pac.estado = self.cleaned_data['estado']
		pac.cidade = self.cleaned_data['cidade']
		pac.endereco = self.cleaned_data['endereco']
		pac.bairro = self.cleaned_data['bairro']
		pac.complemento = self.cleaned_data['complemento']
		pac.save()'''
		
	
		                  
			

class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    '''error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }'''
    new_password1 = forms.CharField(label=_("*Nova Senha"),
                                    widget=forms.PasswordInput(attrs = {'class':'form-control','required':'true'}))
    new_password2 = forms.CharField(label=_("*Confirme a senha"),
                                    widget=forms.PasswordInput(attrs = {'class':'form-control','required':'true'}))

    def __init__(self, user,*args, **kwargs):
        self.user = user
        super(SetPasswordForm, self).__init__(*args, **kwargs)


    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                   'Senhas nao correspondem'
                )
        return password2

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save()
        return self.user

    def clean_new_password1(self):
		password = self.cleaned_data['new_password1']
		if len(password) < 6:
			raise forms.ValidationError('Senha deve conter no mínimo 6 caracteres')
		return password


class PasswordChangeForm(SetPasswordForm):
	

    old_password = forms.CharField(label=_("*Senha atual"),
                                   widget=forms.PasswordInput(attrs = {'class':'form-control','required':'true'}))
    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                'Senha atual incorreta'
            )
        return old_password

    def clean_new_password2(self):
    	new_pass = self.cleaned_data['new_password1']
    	confirm_pass = self.cleaned_data['new_password2']
    	if new_pass != confirm_pass:
    		raise forms.ValidationError('Senhas não correspondem!')
    	elif len(new_pass)<6:
    		raise forms.ValidationError('Senha deve conter no mínimo 6 caracteres')
		return new_pass




class EmailSentForm(PasswordResetForm):


	def clean_email(self):
		email = self.cleaned_data['email']
		if not User.objects.filter(username = email).exists():
			raise forms.ValidationError('email não cadastrado')
		return email
		

class RotaForm(ModelForm):
	class Meta:
		model = Rota
		exclude = ('profissional',)
	

	def __init__(self,*args,**kwargs):
		self.user = kwargs.pop('user',None)
		#self.bairro_id = Bairro.objects.filter(cidade = Profissional.objects.get(user = self.user).cidade.id)
		super(RotaForm,self).__init__(*args,**kwargs)
		self.fields['bairro'] = forms.ModelChoiceField(widget = forms.Select(attrs = {'class':'form-control','required':'true'}), queryset = Bairro.objects.filter(cidade = Profissional.objects.get(user = self.user).cidade.id))
		#self.fields['bairro'].widget = forms.Select(attrs = {'class':'form-control','required':'true'})
	
	
	
	

	def clean_bairro(self):
		prof = Profissional.objects.get(user = self.user)
		rota = Rota.objects.filter(profissional = prof, bairro = self.cleaned_data['bairro'])
		if rota.exists():
			raise forms.ValidationError('Rota com bairro ja existente!')
		return self.cleaned_data['bairro']
	
class FiltroAreaForm(forms.Form):
	area = forms.ModelChoiceField( widget=forms.Select(attrs = {'class':'form-control','required':'true'}),queryset = Area.objects.all(),label="*Area:")


# class ChangePhoto(ModelForm):
# 	class Meta:
# 		model = Profissional
# 		exclude = ('user','email','slug','key_expires','activation_key','nome','email','cep','estado',
# 			'cidade','endereco','bairro','complemento','area')		

class ServicosAddForm(ModelForm):
	class Meta:
		model = Servicos
		exclude =('profissional',)
	
	def __init__(self,*args,**kwargs):
		self.user = kwargs.pop('user',None)
		super(ServicosAddForm,self).__init__(*args,**kwargs)

	descricao = forms.CharField(label='Descricao',widget=forms.TextInput(attrs = {'class':'form-control','required':'true'}))

	def clean_descricao(self):
		prof = Profissional.objects.get(user = self.user)
		descricao = Servicos.objects.filter(profissional = prof,descricao = self.cleaned_data['descricao'])
		if descricao.exists():
			raise forms.ValidationError('Serviço já adicionado')
		return self.cleaned_data['descricao']

#from siasdo.consultas.models import ConsultaPaciente
class ServicoRequest(forms.Form):
	# class Meta:
	# 	model = ConsultaPaciente
	# 	exclude = ('profissional','paciente','status_consulta',)
	msg = forms.CharField(widget = forms.Textarea(attrs = {'class':'form-control','required':'true'}),label = 'Peça aqui sua consulta!')


class ResetEmailForm(forms.Form):
	email = forms.CharField(widget = forms.TextInput(attrs = {'class':'form-control','required':'true'}),label = 'Peça aqui sua consulta!')

	def clean_email(self):
		email = self.cleaned_data['email']
		if not User.objects.filter(username = email).exists():
			raise forms.ValidationError('')
		return email	
class GeralSearchForm(forms.Form):
	busca = forms.CharField(widget = forms.TextInput(attrs={'class':'searchbox-input','required':'true','placeholder':
		'Procurar ...','onkeyup':'buttonUp();'}))
	# def clean(self):
	# 	return self.cleaned_data['busca']




# class SetPasswordForm2(SetPasswordForm):
# 	def clean_new_password2(self):
# 		password1 = self.cleaned_data.get('new_password1')
# 		password2 = self.cleaned_data.get('new_password2')
# 		if password1 and password2:
# 			if password1 != password2:
# 				raise forms.ValidationError(
# 					'Senhas nao correspondem'
# 					)
# 				return password2

# 	def clean_new_password1(self):
# 		password = self.cleaned_data['new_password1']
# 		if len(password) < 6:
# 			raise forms.ValidationError('Senha deve conter no mínimo 6 caracteres')
# 			return password
class ChangeFoto(forms.Form):
	
	foto = forms.ImageField(widget =forms.FileInput(attrs = {'accept':'image/jpeg,image/jpg,image/png'}),required = True)


class Avalie(forms.Form):
	#one = forms.ChoiceField()
	nota = forms.IntegerField(widget = forms.NumberInput(attrs = {'class':'form-control','min':'0','max':'10','required':'true',
		'step':'0.5'}),label = u'Dê uma nota:')
	msg = forms.CharField(widget = forms.Textarea(attrs={'class':'form-control','required':'true'}),label="Deixe uma sugestão:")

