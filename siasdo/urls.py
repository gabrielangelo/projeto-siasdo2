# coding=utf8
# -*- coding: utf8 -*-
# vim: set fileencoding=utf8 :
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings
from siasdo.site_siasdo import views
from siasdo.site_siasdo import forms
from django.contrib.auth.decorators import login_required
from siasdo.usuarios import views as p 
#from siasdo.estados import views as teste
urlpatterns = patterns('',
    # Examples:
     url(r'^$', 'siasdo.site_siasdo.views.home', name='home'),
     url(r'^cadastre-se/$',views.cadastre_se,name = 'cadastre_se'),
     url(r'^profissionais/$', 'siasdo.site_siasdo.views.profissionais', name='profissionais'),
     url(r'^pacientes/$', 'siasdo.site_siasdo.views.pacientes', name='pacientes'),
     url(r'^password/$', 'siasdo.site_siasdo.views.password', name='password'),
     url(r'^success/$', 'siasdo.site_siasdo.views.success', name='success'),
     url(r'^login/$', 'siasdo.site_siasdo.views.LoginRequest', name='login'),
     # url(r'^loginproblem/$',views.login_problem,name = 'problem_login'),
     url(r'^search/prof/$', 'siasdo.site_siasdo.views.search_page',name = 'search'),
     url(r'^search/profissional/(?P<slugprof>.*)/$','siasdo.site_siasdo.views.single_profissional',name = 'single_profissional'), 
     url(r'^error_page/$', 'siasdo.site_siasdo.views.error_page', name ='error_page'),
     url(r'^avalie/$', 'siasdo.site_siasdo.views.avalie', name ='avalie'),
     #url(r'^index_logado/$', 'siasdo.site_siasdo.views.index_logado', name ='index_logado'),
     url(r'^resend_email/$',views.resend_email,name = 'resend_email'),
     
     
     #url(r'^profile_profissional/edit_photo/$','siasdo.site_siasdo.views.changephoto',name ="edit_photo"),

     #url para view profile que redirecionar para as devidas paginas
     url(r'^profile/$', 'siasdo.site_siasdo.views.profile',name = 'profile'),
     url(r'^profile/search/$','siasdo.site_siasdo.views.profile_redirect_search',name = 'profile_search'),
     url(r'^logout/$', 'siasdo.site_siasdo.views.LogoutRequest',name = 'logout'),
     #urls dos perfis de profissionais
     url(r'^register/success_register/$','siasdo.site_siasdo.views.success_register',name = 'success_register'),
     url(r'^accounts/confirm/(?P<activation_key>\w+)/$','siasdo.site_siasdo.views.register_confirm_email', name = 'confirm_email'),
     
     url(r'^profile_profissional/delete_consultas/(?P<id_consulta>\d+)/$',views.delete_consultas_profissional,name = 'delete_consultas_prof'),
     url(r'^profile_profissional/add_servico/$',views.add_servico,name = 'add_servico'),
     url(r'^profile_profissional/delete_servico/(?P<pk_servico>\d+)/$',views.delete_servico,name = 'delete_servico'),
      url(r'^profile_profissional/move_consulta/(?P<id_consulta>\d+)/$',views.move_consultas,name = 'move'),
     url(r'^profissional/curriculo/(?P<idprof>\d+)/$',views.get_curriculo,name = 'get_curriculo'),
     url(r'^profile_profissional/persons/$','siasdo.site_siasdo.views.profile_profissional_list',name = 'profile_persons'),
     url(r'^profile_profissional/search/$','siasdo.site_siasdo.views.profile_profissional_search',name = 'profile_profissional_search'),
     url(r'^profile_profissional/delete_rote/(?P<id_rote>\d+)/$','siasdo.site_siasdo.views.delete_rote',name = 'delete_rote'),
     url(r'^profile_profissional/delete_rote_page/$','siasdo.site_siasdo.views.delete_rote_page',name = 'delete_rote_page'),
     url(r'^profile_profissional/add_rota/$','siasdo.site_siasdo.views.add_rote',name = 'add_rota'),
     url(r'^profile_profissional/$','siasdo.site_siasdo.views.profile_profissional',name = 'profile_profissional'),
     url(r'^profile_profissional/edit_settings/$',views.EditSettingsProfissional,name = 'edit_settings_profissional'),
     url(r'^profile_profissional/edit_settings/success/$','siasdo.site_siasdo.views.success_config_profile',name = 'succes_config'),
     url(r'^profile_profissional/confirm_query/(?P<id_consulta>\d+)/$',views.confirmar_consulta,name = 'confirmar'),
     url(r'^profile_profissional/edit_senha/$','django.contrib.auth.views.password_change',{'template_name':
          'alterar_senha.html','post_change_redirect':'/profile_profissional/edit_senha/success/','password_change_form':forms.PasswordChangeForm},name = 'edit_senha'),
     url(r'^profile_profissional/edit_senha/success/$','django.contrib.auth.views.password_change_done',
          {'template_name':'change_password_succes.html'}),
     url(r'^profile_profissional/consultas_nao_confirmadas/$',views.consultas_profissional_nao_confirmadas,name = 'consultas_nao_confirmadas'),
     url(r'^profile_profissional/change_foto/$',views.change_foto,name = 'change_foto'),
     #url dos perfis dos pacientes
     url(r'^profile_paciente/delete_consultas/(?P<id_consulta>\d+)$',views.delete_consultas_paciente,name = 'delete_consultas_paciente'),
     url(r'^profile_paciente/$', 'siasdo.site_siasdo.views.profile_paciente', name = 'profile_paciente'),
     url(r'^profile_paciente/edit_settings/$',views.EditSettingsPaciente,name = 'edit_settings_paciente'),
     url(r'^profile_paciente/search/$','siasdo.site_siasdo.views.profile_paciente_search',name = 'profile_paciente_search'),
     url(r'^profile_paciente/minhas-consultas/$',views.minhas_consultas,name = 'minhas_consultas'),
     url(r'^profile_paciente/edit_senha/$','django.contrib.auth.views.password_change',{'template_name':
          'alterar_senha2.html','post_change_redirect':'/profile_paciente/edit_senha/success/',
          'password_change_form':forms.PasswordChangeForm},name = 'edit_senha_paciente' ),
     url(r'^profile_paciente/edit_senha/success/','django.contrib.auth.views.password_change_done',name = 'success_senha_paciente'),
     url(r'^search/$', 'siasdo.site_siasdo.views.search', name = 'search'),
    
     #urls para recuperaçao de senha via email
     url(r'^resetpassword/passwordsent/$', 'django.contrib.auth.views.password_reset_done',{'template_name':'success.html'},name='password_reset_done'),
     url(r'^resetpassword/$', 'django.contrib.auth.views.password_reset',{'template_name':'forgot_password.html', 'password_reset_form':forms.EmailSentForm},name = 'password_reset'),
     url(r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm',{'template_name':'confirm_email.html'},name = 'password_reset_confirm'),
     url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete',{'template_name':'success_password.html'},name ='password_reset_complete'),
     url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$','django.contrib.auth.views.password_reset_confirm',{'template_name':'confirm_email.html',
          'set_password_form':forms.SetPasswordForm},name='password_reset_confirm'),   
     #urls para recuperacao de senha via email
     #url(r'^config_profile/$', 'siasdo.site_siasdo.views.config_profile', name='config_profile'),
     #url(r'^message/$', 'siasdo.site_siasdo.views.message', name='message'),
     # url(r'^alterar_senha/$','siasdo.site_siasdo.views.alterar_senha', name = 'alterar_senha'),
     #url(r'^blog/', include('blog.urls')),
    #url(r'^teste/',teste.teste,name = 'teste'),
    url(r'^get_bairros/',views.get_bairros,name = 'get'),   
    url(r'^admin/', include(admin.site.urls)),
    )


